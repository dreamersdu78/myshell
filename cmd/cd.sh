cd2 () {
    path=$1

    if [[ $path = "" ]]
    then
        pushd $HOME
        return
    fi

    if [[ $path = ".." ]]
    then
        pushd ..
        return
    fi

    if [ -a "$PWD/$path" ]
    then
        pushd $path
        return
    else
        mkdir -p $path
        pushd $path
    fi
}