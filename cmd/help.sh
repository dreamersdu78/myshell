#!/bin/bash
source $(pwd)/.env

help () {
    if [[ $# == 0 ]]
    then
        afficheHelp
    else
        echo -e "launch 'man ls' for read info $2 command or command not found"
    fi

}

afficheHelp () {
    echo -e "myPrompt, $VERSION for bash@$BASH_VERSION"
    echo -e "These shell commands are defined internally.  Type \`help' to see this list."
    echo -e "Type \`help name' to find out more about the function \`name\'."
    echo -e "Use \`info bash' to find out more about the shell in general."
    echo -e "Use \`man [command]' or \`info' to find out more about commands not in this list."
    echo ""
    echo -e "A star (*) next to a name means that the command is disabled"
    echo -e "ls [-lacdh] [dir | file] : listing direcly folders and files"
    echo -e "version : getting myPrompt versions"
    echo -e "help : affiche listing command usable in this shell"
}