#!/bin/bash
source $(pwd)/utils/path.sh
source $(pwd)/utils/get-info.sh

# 
#   This function is set flag for decter use -* for ls2 function
#   
#   Params $1: string for content one string - any carataire
#
optionSetForLs () {
    case $1 in 
        a | A)
            export AFLAG=true;
        ;;
        l | L)
            export LFLAG=true;
        ;;
        c)
            export CFLAG=true;
        ;;
        d)
            export DFLAG="-e"
        ;;
        h)
            cat ${PWD}/man/ls-short.md
            echo ""
            export HFLAG=true
        ;;
        * )
            echo -e "option $1 is not found"
        ;;
    esac
}

# unset all export variable use in the ls2 function
optionUnsetForLs () {
    unset AFLAG
    unset LFLAG
    unset CFLAG
    unset DFLAG
    unset HFLAG
}

setDefaultFlag () {
    export DFLAG="-"
}


#
#   ls2 is simple simularie function of ls shell 
#   
#   $@ : array<string> is content path and option params for exemple = ("." "-l" "-a")
#
ls2 () {
    path=$(clearPath $(pwd))
    isPathSet=false
    setDefaultFlag

    for params in $@
    do
        if [[ "${params:0:1}" = "-"  &&   "${params:1:1}" != "-" ]]
        then
            optionSetForLs ${params:1:1}

            if [[ ${#params} > 2 ]]
            then
                for (( i=2; i<${#params}; i++))
                do
                    optionSetForLs ${params:i:1}
                done
            fi
        elif [ !$isPathSet ]
        then
            path=$params
            isPathSet=true
        fi
    done

    if [ $HFLAG ]
    then
        optionUnsetForLs
        return
    fi

    if [[ -f "$path" ]]
    then
        echo "$path this folder not found"
        return
    fi

    if [ $CFLAG ]
    then
        echo "total=$(getCountWithPath $path)"
    fi

    if [ $AFLAG ]
    then
        for files in $path/.* 
        do
            if [[ $LFLAG ]]
            then
                echo ${DFLAG}n "$(getInfo $path) "
                echo ${DFLAG}n "$(clearForPrintPath $files)"
                echo ""
            else
                echo ${DFLAG}n "$(clearForPrintPath $files) "
            fi
        done
    fi

    
    for files in  $path/*
	do
        if [[ $LFLAG ]]
        then
            echo ${DFLAG}n "$(getInfo $files) "
            echo ${DFLAG}n $(clearForPrintPath $files)
            echo ""
        else
            echo ${DFLAG}n "$(clearForPrintPath $files) "
        fi
	done

    if [[ ! $LFLAG ]]
    then
        echo ""
    fi

    optionUnsetForLs 
}

