#1/bin/bash
optionSetMan () {
	case $1 in
		k)
			export KFLAG=true
		;;
		p)
			export PFLAG=true
		;;
		h)
			cat $APPHOMEPATH/man/man-short.md
			echo "";
			return 1
		;;
		*)
			echo -e "option $1 is not found"
			return 1
		;;
	esac
}

optionUnsetForMan () {
	unset KFLAG
	unset PFLAG
}

printFileForMan () {
	if [ $PFLAG ]
	then
		cat $1
		echo ""
	else 
		more $1
	fi
}

getNameFileForMan () {
	name=$1
	
	if [ $KFLAG ]
	then
		echo -e "$name-short.md"
	else
		echo -e "$name-man.md"
	fi
}

man2 () {
	isNameSet=false
	nameFile=""

	for params in $@
	do
		if [[ ${params:0:1} == "-" ]]
		then
			for (( i=1; i < ${#params}; i++ ))
			do
				optionSetMan ${params:i:1}

				if [[ $? == 1 ]]
				then 
					return
				fi
			done
		else
			if [ !$isNameSet ]
			then
				isNameSet=true
				nameFile=$params
			fi
		fi
	done
	
	nameFile="$APPHOMEPATH/man/$(getNameFileForMan $nameFile)"
	echo $nameFile
	if [ -f $nameFile  ]
	then
		printFileForMan $nameFile
	fi

	optionUnsetForMan
}
