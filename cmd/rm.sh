#!/bin/bash
optionSetForRm () {
    case $1 in
		r)
			export RFLAG=true
		;;
		v)
			export VFLAG=true
		;;
        f)
            export FFLAG=true
        ;;
		h)
			cat $APPHOMEPATH/man/rm-short.md
			echo "";
			return 1
		;;
		*)
			echo -e "option $1 is not found"
			return 1
		;;
	esac
}

optionUnsetForRm (){
	unset RFLAG
	unset VFLAG
	unset FFLAG
}

deleteDirector () {
	path=$1

	isEmpty=$(ls $path)

	if [ -d $path ] & [[ $isEmpty = "" ]]
	then
		rmdir $path
		return
	fi
	
	for files in $pasth
	do
		if [ $VFLAG  ]
		then
			echo $files
		fi

		isEmpty=$(ls $path)


		if [ -d $path || ! [ $isEmpty = "" ]]
		then
			deleteDirector $files
			return
		fi

		if [ ! $FFLAG ] & [ -d $files ]
		then 
			read "Are you sure to delete this folder ? [y/n]" response
			
			if [[ $response = "n" ]]
			then
				return 1
			fi
		fi

		if [ -d $files ]
		then
			echo $files
			rmdir $files
		fi

		if [ -f  $files ]
		then
			unlink $files
		fi
	done

	return 0
}

rm2() {
	path=""

	for params in $@
	do
		if [[ ${params:0:1} = '-' ]]
		then
			for (( i=1; i<${#params}; i++ ))
			do
				optionSetForRm ${params:i:1}

				if [[ $? = 1 ]]
				then
					return
				fi
			done
		else
			if [[ $path = "" ]]
			then
				path=$params
			fi
		fi
	done

	if [ ! -e $path ] 
	then
		echo "is not exits files or directory"
		optionUnsetForRm
	fi

	if [ -d $path ]  & [ $RFLAG ]
	then
		deleteDirector $path
		optionUnsetForRm
		return
	elif [ -d $path & ! $RFLAG ]
	then
		echo "-r for delete the directory"
	fi

	if [ -f $path ]
	then
		unlink $path
	fi

	optionUnsetForRm
}
