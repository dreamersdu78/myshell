#!/bin/bash
optionSetForRm () {
    case $1 in
		P)
			export PFLAG=true
		;;
		v)
			export VFLAG=true
		;;
		h)
			cat $APPHOMEPATH/man/rm-short.md
			echo "";
			return 1
		;;
		*)
			echo -e "option $1 is not found"
			return 1
		;;
	esac
}

optionUnsetForRmdir (){
	unset PFLAG
	unset VFLAG
}


rmdir2 () {
    path=""

	for params in $@
	do
		if [[ ${params:0:1} = '-' ]]
		then
			for (( i=1; i<${#params}; i++ ))
			do
				optionSetForRmdir ${params:i:1}

				if [[ $? = 1 ]]
				then
                    optionUnsetForRmdir
					return
				fi
			done
		else
			if [[ $path = "" ]]
			then
				path=$params
			fi
		fi
	done

    isEmpty=$(ls $path)

    if [ -d $path ] & [[ $isEmpty = "" ]]
    then
        if [ $VFLAG ]
        then  
            echo $PWD/$path
        fi
        
        rm -r $path
    fi

    if [ $PFLAG ] & [ -d $path ]
    then
        rm -rf $path
    fi

    optionUnsetForRmdir
}