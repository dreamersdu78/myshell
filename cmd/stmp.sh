optionSetForStmp () {
    case $1 in 
        d )
            export LASTFLAG=$1;
            break
        ;;
        o )
            export LASTFLAG=$1;
            break
        ;;
        m )
            export LASTFLAG=$1;
            break
        ;;
        h)
            cat ${PWD}/man/stmp-short.md
            echo ""
            export HFLAG=true
        ;;
        * )
            echo -e "option $1 is not found"
        ;;
    esac
}

stmp () {
    mkdir tmp/

    for params in $@
    do
        if [[ "${params:0:1}" = "-"  ]]
        then
            optionSetForStmp ${params:1:1}
        else
            if [[ $LASTFLAG = "d" ]]
            then
                addressee=$params
            fi

            if [[ $LASTFLAG = "o" ]]
            then
                object=$params
            fi

            if [[ $LASTFLAG = "m" ]]
            then
                echo $params > $APPHOMEPATH/tmp/message.txt
            fi

        fi
    done

    if [ ! $addressee ] && [ ! $object ] && [ ! -f $APPHOMEPATH/tmp/message.txt ]
    then
        echo "your mail is incomplet valid exemple stmp -o \"is a object mail\" -d \"test@test.com\" -m \"your message\""
        return
    fi

    mail -s "$object" $addressee < tmp/message.txt

    rm -rf tmp/
}