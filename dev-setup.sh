#!/bin/bash
clear

if command -v npm > /dev/null
then
    echo -e "[✅]: npm detected"
else 
    echo "[❌]: please install npm with nodejs"
    exit
fi

if command -v nodemon > /dev/null
then
    echo -e "[✅]: nodemon detected"
else 
    echo -e "[❌]: install nodemon with root authorize"
    echo -e "launch : sudo npm i -g nodemon and relaunch this"
fi

nodemon --watch ./* --watch ./**/* --exec "./my-prompt.sh"

