NAME
     ls – list directory contents

SYNOPSIS
     ls [-l Flag ] [file ...]

DESCRIPTION
     For each operand that names a file of a type other than directory, ls displays its name as well as any requested, associated information.  For
     each operand that names a file of type directory, ls displays the names of files contained within that directory, as well as any requested,
     associated information.

     If no operands are given, the contents of the current directory are displayed.  If more than one operand is given, non-directory operands are
     displayed first; directory and non-directory operands are sorted separately and in lexicographical order.

     The following options are available:

     -c      print a total files find exemple : total=#ListFiles

     -l      (The lowercase letter “ell”.) List files in the long format, as described in the The Long Format subsection below.

     -a      Include directory entries whose names begin with a dot (‘.’).

     -b     Print the specifique carataire

     -h      print help notice

EXEMPLE
    ls : cmd dev-setup.sh man my-prompt.sh utils
    ls -l : 
        drwxr-xr-x@ 4 dev  staff  128 Sep 17 05:48 cmd
        -rwxr-xr-x@ 1 dev  staff  423 Sep 17 05:48 dev-setup.sh
        drwxr-xr-x@ 3 dev  staff   96 Sep 17 05:48 man
        -rwxr-xr-x@ 1 dev  staff  676 Sep 17 05:48 my-prompt.sh
        drwxr-xr-x@ 4 dev  staff  128 Sep 17 05:48 utils
    ls -a : . .. cmd dev-setup.sh man my-prompt.sh utils
    ls -c : 
        total=16
        cmd dev-setup.sh man my-prompt.sh utils