
NAME
     man, apropos, whatis – display online manual documentation pages

SYNOPSIS
     man [-dh] [-P pager] page ...

DESCRIPTION
     The man utility finds and displays online manual documentation pages.  If mansect is provided, man restricts the search to the specific
     section of the manual.

     The sections of the manual are:
           1.   print a Global Manual
           2.   print a short Manual

     Options that man understands:  
        -d      Print extra debugging information.  Repeat for increased verbosity.  Does not display the manual page.
