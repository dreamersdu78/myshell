
NAME
     rm, unlink – remove directory entries

SYNOPSIS
     rm [-f | -i] [-dIRrvWx] file ...
     unlink [--] file

DESCRIPTION
     The rm utility attempts to remove the non-directory type files specified on the command line.  If the permissions of the file do not permit writing, and the
     rm [-f | -i] [-dIRrvWx] file ...
     unlink [--] file

DESCRIPTION
     The rm utility attempts to remove the non-directory type files specified on the command line.  If the permissions of the file do not permit writing, and the
     standard input device is a terminal, the user is prompted (on the standard error output) for confirmation.

     The options are as follows:
     
     -r   delete a derectory   
     -f   not print a confirmate a delte derectory content other file
     -f   print help for man
     -v   print a verbose info why is delete