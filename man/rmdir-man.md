NAME
     rmdir – remove directories

SYNOPSIS
     rmdir [-pv] directory

DESCRIPTION
     The rmdir utility removes the directory entry specified by each directory argument, provided it is empty.

     Arguments are processed in the order given.  In order to remove both a parent directory and a subdirectory of that parent, the subdirectory must be specified
     first so the parent directory is empty when rmdir tries to remove it.

     The following option is available:

     -p      Each directory argument is treated as a pathname of which all components will be removed, if they are empty, starting with the last most component.
             (See rm(1) for fully non-discriminant recursive removal.)

     -v      Be verbose, listing each directory as it is removed.
