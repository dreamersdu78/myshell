rmdir [-pv] directory
     -p      Each directory argument is treated as a pathname of which all components will be removed, if they are empty, starting with the last most component.
     -v      Be verbose, listing each directory as it is removed.
