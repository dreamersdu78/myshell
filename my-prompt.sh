#!/bin/bash
source .env
source cmd/ls.sh
source cmd/help.sh
source cmd/man.sh
source cmd/rm.sh
source cmd/rmdir.sh
source cmd/age.sh
source cmd/quit.sh
source cmd/cd.sh
source cmd/pwd.sh
source cmd/hour.sh
source cmd/httpget.sh
source cmd/stmp.sh
source cmd/open.sh
source cmd/createUser.sh

export APPHOMEPATH=$PWD

main(){
    clear
    name=$USER;


    while [ 1 ]; do
        echo -ne "$(pwd) ~ ${name} : ";
        read command option;

        cmd $command "${option}"
    done
}

cmd(){
    cmd=$1
    option=$2

    case "${cmd}" in
        ls | ls2)
            ls2 $option
        ;; 
        version)
            echo $VERSION
        ;;
        help)
            help $option
        ;;
        man )
            man2 $option 
        ;;
        rmdir | rmd )
            rmdir2 $option
        ;;
        rm )
            rm2 $option
        ;;
        age )
            age
        ;;        
        quit )
            quit
        ;;
        cd )
            cd2 $option
        ;;
        pwd )
            pwd2
        ;;
        hour )
            hour
        ;;
        httpget )
            httpget $option
        ;;
        stmp )
            stmp $option
        ;;
        open )
            open2 $option
        ;;
        createUser )
            createUser
        ;;
        *)
            echo -e "your command is not found."
        ;;
    esac
}

main
