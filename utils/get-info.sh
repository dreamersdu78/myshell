#!/bin/bash

#
#   get permision with nb permision for exemple 755 = rwxr-xr-x
#
#   $1 : number is content permision files 
#
getPremmision(){
    nbPremision="$1"
    caratairePerm=""

    for (( i=0; i<${#nbPremision}; i++ ))
    do
        nb=${nbPremision:i:1}
        caratairePerm=$caratairePerm$(getCaractaireByPerm $nb);
    done

    echo $caratairePerm
}

#
#   get permision with the one nb of permision files for exemple 7 = rwx
#
#   $1 : number is content one nb of the permision files
#
getCaractaireByPerm () {
    permNb=$1
    restPerm=$(($permNb-4))

    if [ $restPerm -ge 0 ]
    then
        echo -n r
    else
        echo -n -
        restPerm=$(($restPerm+4))
    fi

    restPerm=$(($restPerm-2))
    if [ $restPerm -ge 0 ]
     then
        echo -n w
    else
        echo -n -
        restPerm=$(($restPerm+2))
    fi

    restPerm=$(($restPerm-1))
    if [ $restPerm -ge 0 ]
     then
        echo -n x
    else
        echo -n -
    fi
}


getInfo () {
    statPerm=$(stat -f "%p" $1)
    
    if [ ! -f $1 ]
    then
        echo -n "d"
    fi

    echo -n "$(getPremmision ${statPerm: -3}) "

    echo -n "$(stat -f "%l" $1) "

    echo -n "$(stat -f "%Su" $1) "

    echo -n "$(stat -f "%Sg" $1) "

    sizeFile=$(stat -f "%z" $1)

    # for -t flag use a man strftime for the format simalari of c function 

    echo -n " $(getCompresedWieght $sizeFile) "

    echo -n "$(stat -f "%Sm" -t "%B %d %H:%M %Y" $1) "
}




getCompresedWieght (){
    wieght=$1
    symbloWieghts=("o" "Ko" "Mo" "Go" "To")
    for i in 4 3 2 1 0
    do  
        rest=$(echo "$wieght/2^($i*10)" | bc)
        if [ $rest -gt 0  ]
        then
            echo ${rest:0:3}${symbloWieghts[i]}
            return
        fi
    done 

    echo "${rest}o"
}